#! /usr/bin/env python

import rospy
from geometry_msgs.msg import Twist

rospy.init_node("moving_from_python")

pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
msg = Twist()
rate = rospy.Rate(1 / 3.0) 
stop = True

while not rospy.is_shutdown():
    msg.angular.z = 0.0 if stop else 5.0
    pub.publish(msg)
    stop = not stop
    rospy.loginfo("Msg published!")
    rate.sleep()